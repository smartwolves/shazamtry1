import java.io.File;


import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;

public class WavtoMp3 {

	public static File convertFromWavToMp3(File src){
		String absolutePath = src.getAbsolutePath();
		String filePath = absolutePath.
		    substring(0,absolutePath.lastIndexOf(File.separator));
		String fileNAme=absolutePath.substring(absolutePath.lastIndexOf(File.separator)+1,absolutePath.lastIndexOf("."));
		File target=new File(filePath+File.separator+fileNAme+".mp3");
		
		AudioAttributes audio = new AudioAttributes();
		audio.setCodec("libmp3lame");
		audio.setBitRate(new Integer(128000));
		audio.setChannels(new Integer(2));
		audio.setSamplingRate(new Integer(44100));
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("mp3");
		attrs.setAudioAttributes(audio);
		Encoder encoder = new Encoder();
		try {
			encoder.encode(src, target, attrs);
		} catch (IllegalArgumentException | EncoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sorry");
		}
		
		return target;
		
		
	}

}
